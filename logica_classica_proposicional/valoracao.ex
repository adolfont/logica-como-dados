# v(p)=1 v(q)=1 v(r)=0

v = %{
  :p => 1,
  :q => 0,
  :r => 1
}

IO.inspect(v, label: "Exemplo de valoração")
