
1. Valoração
2. Extensão de valoração
3. Aplicação de valoração a fórmula
4. Tabela-verdade
5. Classificação de fórmulas
6. Consequência lógica - duas fórmulas
7. Consequência lógica - mais do que duas fórmulas
8. Equivalência lógica
9. Novos conectivos
