# tabela-verdade para p&q

tv = [
  [:p, :q, {:and, :p, :q}],
  [0, 0, 0],
  [0, 1, 0],
  [1, 0, 0],
  [1, 1, 1]
]

IO.inspect(tv, label: "Exemplo de tabela-verdade")
